# vue动态流程图

## 一 项目说明
* 该项目使用topology插件，使用vue进行改编，可用于制作动态流程图、组态图等前端插件，渲染以及图表保存数据均为json格式数据，可以输出为png、svg，可以直接打开json流程图文件。
* 官方效果图中模板可以直接下载为json文件然后导入，但需要注意的是图元组件保持一致，否则加载数据会显示不全。
* 注意：本人对topology-vue的插件源码进行了改动，去除了菜单栏某些功能（LOGO、分享、登录等等），根目录有zip包，如果需要，解压直接放到node_modules文件夹中，全部替换原有topology-vue包即可。
## 二 项目启动
### 1.安装依赖
```
npm install
```
### 2.启动
```
npm run serve
```
### 3.打包
```
npm run build
```
### 4.当前效果图
当前效果图
![当前效果图](https://gitee.com/wenxiaod/animation-flow/raw/master/src/assets/animateFlow.png)
![当前效果图](https://gitee.com/wenxiaod/animation-flow/raw/master/src/assets/animateFlow.gif)

